<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrito de compras</title>
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
 <div class="d-flex justify-content-center mt-4">
    <h1>Carrito de compras</h1>
 </div>
  <div class="container">
   <div class="container bg mt-4">
    <div class="row">
    <table class="table">
    <thead class="thead-dark">
      <tr>
        
        <th scope="col">IMAGEN</th>
        <th scope="col">TITULO</th>
        <th scope="col">PRECIO</th>
        <th scope="col">
          OPCIONES
        </th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td><img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png?fbclid=IwAR3vVJGUQQMSMMmU6FB-o4HmHBMCqC6S6JasBfUEqspZRXYC9aesgOt319Q"
          alt="logopincerolas" height="150x"></td>
        <td>Logo Pincerolas</td>
        <td>$150 mxn</td>
        <td>
          <button class="btn btn-danger" type="">Eliminar</button>
        </td>
      </tr>
    </tbody>
  </table>

  <div class="container ">
    <h5 class="text-center">Total: $150 MXN</h5>
  </div>


  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col">
        <button class="btn btn-dark">Vaciar todo</button>
      </div>
      <div class="col d-flex justify-content-left">
        <button class="btn btn-danger">Comprar</button>
      </div>
    </div>
  
</div><br>

</body>
</html>












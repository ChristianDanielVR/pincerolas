<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>¿Como funciona?</title>
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
    #a{
      text-decoration: none;
    }
  </style>
</head>
<body>

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="inicio">
            <img src="../../configuracion/css/logo1.png" alt="Logore" style="width:40px;" class="rounded-pill"> 
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Categorias</a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="bebidas">Bebidas</a></li>
                  <li><a class="dropdown-item" href="mariscos">Mariscos</a></li>
                  <li><a class="dropdown-item" href="pastas">Pastas</a></li>
                  <li><a class="dropdown-item" href="guisados">Guisados</a></li>
                </ul>
              </li>
            </ul>
            <a style="color:#FFFFFF8C;text-decoration:none;" href="caracteristicas.html">¿Quienes somos?</a> 
            <a style="color:#FFFFFF8C;text-decoration:none;padding-left:2%" href="correo">    Comunicate con nosotros</a>
          </div>
        </div>
      <button style="margin-right:5%;width: 10%;" class="btn btn-warning" onclick="cerrar()">Cerrar sesion</button>  
      
      <script src="../../configuracion/js/cerrar_sesion.js"></script>
      </nav>






<br>

    <div id="texto" class="row justify-content-center">
      <div class="col-md-6">
        <h1 id="titulo">¿Como funciona?</h1>
        <p>Bienvenido a Pincerolas donde encontraras diferentes obras tanto a lapiz como cuadros a base de acrilicos</p>
        <p>Puede que no estes muy familiarizado con las pagina asi que aqui te enseñaremos como utilizarla y como navegar</p>
      <div>

        <div id="accordion">

          <div class="card">
            <div class="card-header">
              <a class="btn" data-bs-toggle="collapse" href="#collapseOne">
                ¿Como puedo navegar por la pagina?
              </a>
            </div>
            <div id="collapseOne" class="collapse show bg" data-bs-parent="#accordion">
              <div class="card-body">
                <p>En Pincerolas podras encontrar diversos apartados como:</p>
                <ul>
                  <li><a href="#">Categorias</a><p>Aqui puedes ver las diferentes categorias que Pincerolas ofrece</p></li>
                  
                  <li><a href="#">Configuracion</a><p>En este apartado puedes cambiar tu informacion como la contraseña, correo electronico y cambiar tu direccion domiciliaria</p>
                    <p class="text-danger">Nota: Solo se puede acceder si tienes la sesion iniciada</p></li>
                  
                  <li><a href="#">Carrito de compras</a><p>En este apartado puedes visualizar todos los productos que deseas comprar o eliminar de tu lista en caso de ya no quererlo</p>
                  <p class="text-danger">Nota: Solo se puede acceder si tienes la sesion iniciada</p></li>
                  
                  <li><a href="#">Informacion de contacto</a><p>Aqui puedes encontrar toda la informacion de contacto desde numero telefonico, correo electronico y redes sociales</p></li>
                  <li><a href="#">¿Quienes somos?</a><p>Aqui puedes encontrar la informacion de Pincerolas y conocer mas sobre nosotros</p></li>
                  <li><a href="#">Iniciar sesion</a>/<a href="#">Registrarte</a><p>Aqui puedes iniciar sesion o en caso de no tener tu cuenta te puedes registrar</p>
                    <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEg0yuEn44Lj3arB5b2-DmBddozRgS6H6yQIBuGYVAfGQSrTmleWmh3IjPCscOjDz05O2bRDr3voaUqM9PRHzzozLvhPH-sQBRmAul6Y9fx50UCI6aVxuUkbOkzdGeZ2BxEEU6cWFeYsR3GEgCb2xo3dNTW3-W2hFtMjUWhMY4KVGY40fFkO4CmOO4pIUA/s320/t4.png" alt="p1" height="350px"></li>
                </ul>

              </div>
            </div>
          </div>
        

          <div class="card">
            <div class="card-header">
              <a class="collapsed btn" data-bs-toggle="collapse" href="#collapseTwo">
                ¿Como me registro?
              </a>
            </div>
            <div id="collapseTwo" class="collapse" data-bs-parent="#accordion">
              <div class="card-body">
                <p>Para registrarse en la pagina de Pincerolas es necesario llenar un formulario que pedira datos como:</p>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">Nombre completo</li>
                  <li class="list-group-item">Correo electronico</li>
                  <li class="list-group-item">Contraseña</li>
                </ul>

                <p>Una vez llenado lo necesario, damos siguiente y listo, podremos disfrutar del contenido de la pagina</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhnmmiVf_V_YEXmXCT70faCI5zknqPK4EgrBN7l-Zn3ybKj8yCD6AUvQWa2u33UeH9XlbC6KHDoZKmtMrLNP3p-TAYyYv1dAtG6xAT97EEXhQbTcS7qw4Cb1ibZktTSr6YfuUZNTU2YXQKAqxU8gVCbRfIss2064KEzScKXzJ4-7XUsQoRu-GHyCfV9Zg/s320/t3.png" alt="p1" height="350px">
                <p>Una vez registrados podra ver todas las obras que Pincerolas puede ofrecer, asi como hacer un pedido, agregar obras para su posterior venta, cambiar sus datos ingresados, etc.</p>
                <p>Si no tienes cuenta registrate  <a href="#">AQUI</a></p>
              </div>
            </div>
          </div>
        

          <div class="card">
            <div class="card-header">
              <a class="collapsed btn" data-bs-toggle="collapse" href="#collapseThree">
                ¿Como añado una pintura al carrito?
              </a>
            </div>
            <div id="collapseThree" class="collapse" data-bs-parent="#accordion">
              <div class="card-body">
                <p>Paso 1: Elegir la obra que le interese</p>
                <p>Paso 2: Dirigirse al boton de "Agregar al carrito"</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhLGKaqRkn59RnPyP2U8cYku1HcGGWmjr1aZqJQhpYxgNuRGKQw6AjpHFS3ScH87l-IP9Zw2t8uQGV7K104jh0Q0EcVgAGZdRdXeu7u8wlLfDK4y1kiCtzr3fiWmwh8z4o8uHTdOnsnVym9x9NuaGw8S9YyAzywGWNU2FRi5z61Ph-Jw2cYnGWyFTqHTg/s320/t1.png" alt="p1" height="350px">
                <p>Paso 3: Ir al carrito para ver la obra</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhpgb4bQCPwnyvVh0-_8oECqNHyTGUMNJ5L37NvJLnDa3MLUg0dshjw8SBgorDg3-ODEyz_yTu5nFs3VHBNxEz7IXV7rkWBLrohz9XGgAS1VJajkrGZgqRUruckTmFYaidrIZF9DDBl6yS-uzUcahFUbrCnQb_B9ibqH8G4VQ_eWkK3o6gHoV_dp73lOQ/s320/t2.png" alt="p1" height="350px">
                <p>Nota: En este apartado el usuario puede comprar la obra o tambien puede eliminar todo el carrito o una sola obra</p>
              </div>
            </div>
          </div>
        
        </div>

    </div> <br><br>



    
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de cuadro</title>
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png" type="image/x-icon">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    
    <div class="container mt-4 bg">
        <div class="row">
          <div class="col m-4">
            <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png" alt="60px" class="img-thumbnail">
            
          </div>
          <div class="col m-4">
            <h1>Logo Pincerolas</h1><br><br>
            <h3>Precio: </h3><p>$150 MXN</p>
            <h3>Descripcion</h3>
            <p>
            Este es el logo de pincerolas el cual fue creado por Pincerolas INC. Tiene los colores de la pagina
            y son los 4 principales.   
            </p>
            <h3>Autor: </h3><p>Pincerolas INC</p>
            <h3>Categoria: </h3><p>Digital</p>
            <div class="d-flex justify-content-left">
                <button class="btn btn-danger ">Comprar</button>
                <button class="btn btn-dark ">Agregar al carrito</button>
            </div>
          </div>
        </div>

</body>
</html>
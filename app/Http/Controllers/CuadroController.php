<?php

namespace App\Http\Controllers;

use App\Models\Cuadro;
use App\Models\Categoria;
use Illuminate\Http\Request;

/**
 * Class CuadroController
 * @package App\Http\Controllers
 */
class CuadroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuadros = Cuadro::paginate();

        return view('cuadro.index', compact('cuadros'))
            ->with('i', (request()->input('page', 1) - 1) * $cuadros->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cuadro = new Cuadro();
        $categorias = Categoria::pluck('nombre','id');
        return view('cuadro.create', compact('cuadro','categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Cuadro::$rules);

        $cuadro = Cuadro::create($request->all());

        return redirect()->route('cuadros.index')
            ->with('success', 'Cuadro created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuadro = Cuadro::find($id);

        return view('cuadro.show', compact('cuadro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuadro = Cuadro::find($id);
        $categorias = Categoria::pluck('nombre','id');
        return view('cuadro.edit', compact('cuadro','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Cuadro $cuadro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuadro $cuadro)
    {
        request()->validate(Cuadro::$rules);

        $cuadro->update($request->all());

        return redirect()->route('cuadros.index')
            ->with('success', 'Cuadro updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cuadro = Cuadro::find($id)->delete();

        return redirect()->route('cuadros.index')
            ->with('success', 'Cuadro deleted successfully');
    }
}

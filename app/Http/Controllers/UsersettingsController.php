<?php

namespace App\Http\Controllers;

use App\Models\Usersettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; //Necesario
use Illuminate\Support\Facades\Hash; //Necesario
use Illuminate\Support\Facades\DB; //Necesario

class UsersettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        return view('configuracion');
    }
    public function changePassword(Request $request){    
        
        $user           = Auth::user();
        $userId         = $user->id;
        $userEmail      = $user->email;
        $userPassword   = $user->password;

        if($request->password_actual !=""){
            $NuewPass   = $request->password;
            $confirPass = $request->confirm_password;
            $name       = $request->name;

                //Verifico si la clave actual es igual a la clave del usuario en session
                if (Hash::check($request->password_actual, $userPassword)) {

                    //Valido que tanto la 1 como 2 clave sean iguales
                    if($NuewPass == $confirPass){
                        //Valido que la clave no sea Menor a 6 digitos
                        if(strlen($NuewPass) >= 6){
                            $user->password = Hash::make($request->password);
                            $sqlBD = DB::table('users')
                                  ->where('id', $user->id)
                                  ->update(['password' => $user->password], ['name' => $user->name]);
                    
                            return redirect()->back()->with('updateClave','La contraseña fue cambiada correctamente.');
                        }else{
                            return redirect()->back()->with('clavemenor','Recuerde la contraseña debe ser mayor a 6 digitos.');
                        }
        
                }else{
                    return redirect()->back()->with('claveIncorrecta','Por favor verifique las contraseñas no coinciden.');
                }
           
            }else{
                return back()->withErrors(['password_actual'=>'La Contraseña no Coinciden']);
            }


        }else{
            $name       = $request->name;
            $sqlBDUpdateName = DB::table('users')
                            ->where('id', $user->id)
                            ->update(['name' => $name]);
            return redirect()->back()->with('name','El nombre fue cambiado correctamente.');;

        }

        

}

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Usersettings $usersettings)
    {
        //
    }

    public function edit(Usersettings $usersettings)
    {
        //
    }

    
    public function update(Request $request, Usersettings $usersettings)
    {
        //
    }

    public function destroy(Usersettings $usersettings)
    {
        //
    }
}

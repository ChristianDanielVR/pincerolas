<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrito de compras</title>
     <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    

</head>
<body>


<?php $__env->startSection('content'); ?>
 <div class="d-flex justify-content-center mt-4">
    <h1>Carrito de compras</h1>
 </div>
  <div class="container">
   <div class="container bg mt-4">
    <div class="row">
    <table class="table">
    <thead class="thead-dark">
      <tr>
        
        <th scope="col">IMAGEN</th>
        <th scope="col">TITULO</th>
        <th scope="col">PRECIO</th>
        <th scope="col">
          OPCIONES
        </th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td><img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png"
          alt="logopincerolas" height="150x"></td>
        <td>Logo Pincerolas</td>
        <td>$150 mxn</td>
        <td>
          <button class="btn btn-danger" type="">Eliminar</button>
        </td>
      </tr>
    </tbody>
  </table>

  <div class="container ">
    <h5 class="text-center">Total: $150 MXN</h5>
  </div>


  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col">
        <button class="btn btn-dark">Vaciar todo</button>
      </div>
      <div class="col d-flex justify-content-left">
        <button class="btn btn-danger">Comprar</button>
      </div>
    </div>
  
</div>

<?php $__env->stopSection(); ?>
    </body>
</html>






<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/carrito.blade.php ENDPATH**/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"> 
   
     
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Open+Sans&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Open+Sans&display=swap" rel="stylesheet"> 

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    
  <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    
        <style>
    body {
            
        font-family: 'Montserrat', sans-serif;
     
        
        align-items: center;
        background-color: #f8f9cb;
    }



    .contenido-principal {
        margin-bottom: 50px;
        display: flex;
        align-items: center;
    }

    .contenido-principal__imagen {
        vertical-align: top;
        margin-right: 20px;
        width: 50%;
        max-width: 550px;
    }

    

    .carousel__contenedor {
        position: relative;
        
    }

    .carousel__anterior,
    .carousel__siguiente {
        position: absolute;
        display: block;
        width: 30px;
        height: 30px;
        border: none;
        top: calc(50% - 35px);
        cursor: pointer;
        line-height: 30px;
        text-align: center;
        background: none;
        color: black;
        opacity: 20%;
    }

    .carousel__anterior:hover,
    .carousel__siguiente:hover {
        opacity: 100%;
    }

    .carousel__anterior {
        left: -30px;
    }

    .carousel__siguiente {
        right: -30px;
    }

    .carousel__lista {
        overflow: hidden;
    }

    .carousel__elemento {
        text-align: center;
        margin: 10px;
    }

    .carousel__indicadores .glider-dot {
        display: block;
        width: 30px;
        height: 4px;
        background: #fff;
        opacity: .2;
        border-radius: 0;
    }

    .carousel__indicadores .glider-dot:hover {
        opacity: .5;
    }

    .carousel__indicadores .glider-dot.active {
        opacity: 1;
    }

    @media  screen and (max-width: 800px) {
        body {
            padding: 38px 0;
        }

        .contenido-principal {
            flex-direction: column;
        }

       .contenido-principal > * {
            width: 100%;
       }

    }
    
    .hr:hover {
        background-color: #FF4B4B;
        color:#ffffff;
    }
    .ban{
        background-image: url("https://fondosmil.com/fondo/35152.jpg");
        height: auto;
        width: auto;
    }
    .imgC{
        width:25vh;
        height: 20vh;
        border-top: 2px solid black;
        border-bottom: 2px solid black;
        border-left: 2px solid black;
        border-right: 2px solid black;
    }
    .Hd3{
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    #cont{
      background-color: #FF4B4B;
    }

    #con2{
      background-color: #373737;
    }

</style>


</head>
<body>
  

  <?php $__env->startSection('content'); ?>
 
    <header class="container-xxl text-white text-center shadow-lg p-3 mb-5" id="cont">
        <div class="container  align-items-center ">
      <br>      
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-line"></div>
            </div>
            
            <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png"
            class="">
        </div><br>
                 
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="contenedor">
                    <h1 style="text-align: center">¡Mira las opciones que tenemos!</h1>
     
                    <div class="carousel">
                        <div class="carousel__contenedor">
                            <button aria-label="Anterior" class="carousel__anterior">
                                <i class="fas fa-chevron-left"></i>
                            </button>
    
                            <div class="carousel__lista">
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjSOQlq5ZwT13xsQhY75aP1iBOA98GMCZK4rcGicSePuvgvxAZxmtoOCjn-L68z98XHzS0ZCBPWTCNtIztAGdMQy9jMN4pdZUUYycFN9B-go7hAx8gEhdLywGPm5lgS-guqixfi0dZC7LbZL-6cNDzaQKslqpAF6bm1_v5Iu-ioH3zKO69nzJTwjYon7g/s320/11.jpg" 
                                    alt="Rebajas">
                                    <p>Fan Art Skrillex</p>
                                </div>
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEifqRGMxI-W_Mw_s4FyI62SpYB1TwSZaQodZjD9uRAR-I8vk186ByF1edkOmaVxG5zn_SJQlQDtUGwxbUD1lTholrWGMWFXeyUULr-fiL1HlLlR8bhYXNKOd0D9IGuqk_hkfay24AnysVI5m-R6RDxSu1jKxpLFHv9R8B1-9LcMkKTjGvT_LBsnKlKT6A/s320/4.jpg" 
                                    alt="Constitution Square - Tower I">
                                    <p>Fan Art - LIL PUMP</p>
                                </div>
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgIshhJ_P6ECcnusDsw9EcpRWJ119FYG9InX1PiDgnmUCBQQ8kJ28SM5jM3tCNBMeL29dVahUMEgKoLJgPltHF-pjNNnhHGtSMxHRbo_J55NSFre-ZPa1vaqpBfgW2-z-nKqi85miQWinRSWtXGzbXv5kB4w7pE-2FPEeyT7Y53dI4Y8GJN7iyLae8GAQ/s320/3.jpg" 
                                    alt="Empire State Building">
                                    <p>Retrato - #1</p>
                                </div>
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgL57RI6gl1Vft0WI_KLZgX4nT4t6_XaKKOsElkuzSR1BXPRbW9pBFFleDN4XrbjBo3JjZdozjTrJYodub6TEH4N7PRJYI2QakhZ5gLH3S9hvhxs_jEYMm94ha-2PJIobBiI6yfu5Nyqk_YgrTseUnHX05J0c6bh5-TT4unx_QH5_X8PZeQMcHWyiod0g/s320/9.jpg"
                                     alt="Harmony Tower">
                                    <p>Retrato - #2</p>
                                </div>
                    
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg"
                                     alt="Empire State Building">
                                    <p>Doodle - #1</p>
                                    </div>
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEj1Qu-vGLeEgs5Su8IaBefjiNg7CO0EwuVO2WINCfIw2Nm-v0C7NicRS-Wx-fUp9_o-GIwACpydJ39lfGA3jve5O-bfmieakHqcInfnplmnAp2OpOXJ6Dhic6vFoXBZ9Ro2-nYLl6XpgwIBni8mRBpAfOFSc6qYEqV_id4QC-ype6xwlbYe9Gzazje30Q/s320/15.jpg" 
                                    alt="Harmony Tower">
                                    <p>Fan Art - Gizmo</p>
                                </div>
                                <div class="carousel__elemento">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjdv88dO8Zz2ore4Od7IioCRU9DTvXC2CGbRRoJC2tfq61NR4jl-Id-m--u3GxfzopBe98K93fZweZLVZ67RLIT_a2XDmmdiQmBBdgiWGt0rvVyoJHlzb_2Nh9SYfnrvTiA9xDVB7X-0R3p343LMSkWncJfYFH4FvzBhMtsKBoplT3OjsDTrzcv26g99Q/s320/10.jpg"
                                     alt="Empire State Building">
                                    <p>Fan Art - Leon S. Kennedy</p>
                                </div>
                                <div class="carousel__elemento">
                                    <img href="" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEi1Xoi6f2_0QlJ2Mhdyr8Sri_ERcII4HfsGfvxogtEqIGqTfVsLTTAeeEtrwj9-ulhL2k0HAEHekvjD0LdMz3qMIA2uqoBBo1329CBinmT4yT6djrIOoHuJZrSfW_j3WnvO314IC73-jh3wpJ9LiYTRQVUzFc0yCpm1biF1m_uogwsU3rYvREJwzusZSw/s320/7.jpg"
                                     alt="Harmony Tower">
                                    <p>Dibujo - Pez</p>
                                </div>
                            </div>
    
                            <button aria-label="Siguiente" class="carousel__siguiente">
                               <i class="fas fa-chevron-right"></i>
                            </button>
                        </div>    
                        <div role="tablist" class="carousel__indicadores"></div>
                    </div>
                  </header>
                <br><br><br>
                <center>
                  <div class="container">
                    <div class="row align-items-start">
                      <div class="col">
                       <h1>Doodles</h1>
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active"><a href="#">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjs310urKzVYkXzaVoFAmXynDnbmcQUsl4bdxoqZcwd_yBMkddYaHTGnl3_hAFLxTW0VsNVy7zX_VJl0u0bQo8Xqde3pWl60zzYb22otdtTZUoYdAHuEhaJMsifcEY6QUIj2vsTub3NyiVJWdLYCHsf3XtWQJBLWQWJWAaP7GGaSP1-k9qJ6Qko5Sz1VQ/s320/1.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhM2azsEyZtffJE6zoqdcmZX4ATjls-QpfjSjehPPv0FNwTrEa-SeWOyz0Zfrje_bCsPbcizVdGmbcVCC6QgZqlDsOEFIxIHX2kNcAJ23E9ScCmPB86DvqIIMLibHXabheFdLRcyF9FXDJN8ltNhJFHhFuBwL5omQ_M5WiC72xXCIMeVt4TVnLCCp_yZQ/s320/13.jpg"
                                 class="d-block w-100" alt="..."></a>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="col">
                        <h1>Retratos</h1>
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active"><a href="#">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjs310urKzVYkXzaVoFAmXynDnbmcQUsl4bdxoqZcwd_yBMkddYaHTGnl3_hAFLxTW0VsNVy7zX_VJl0u0bQo8Xqde3pWl60zzYb22otdtTZUoYdAHuEhaJMsifcEY6QUIj2vsTub3NyiVJWdLYCHsf3XtWQJBLWQWJWAaP7GGaSP1-k9qJ6Qko5Sz1VQ/s320/1.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhM2azsEyZtffJE6zoqdcmZX4ATjls-QpfjSjehPPv0FNwTrEa-SeWOyz0Zfrje_bCsPbcizVdGmbcVCC6QgZqlDsOEFIxIHX2kNcAJ23E9ScCmPB86DvqIIMLibHXabheFdLRcyF9FXDJN8ltNhJFHhFuBwL5omQ_M5WiC72xXCIMeVt4TVnLCCp_yZQ/s320/13.jpg"
                                 class="d-block w-100" alt="..."></a>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="col">
                        <h1>Cuadros</h1>
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active"><a href="#">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjs310urKzVYkXzaVoFAmXynDnbmcQUsl4bdxoqZcwd_yBMkddYaHTGnl3_hAFLxTW0VsNVy7zX_VJl0u0bQo8Xqde3pWl60zzYb22otdtTZUoYdAHuEhaJMsifcEY6QUIj2vsTub3NyiVJWdLYCHsf3XtWQJBLWQWJWAaP7GGaSP1-k9qJ6Qko5Sz1VQ/s320/1.jpg" 
                                class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhM2azsEyZtffJE6zoqdcmZX4ATjls-QpfjSjehPPv0FNwTrEa-SeWOyz0Zfrje_bCsPbcizVdGmbcVCC6QgZqlDsOEFIxIHX2kNcAJ23E9ScCmPB86DvqIIMLibHXabheFdLRcyF9FXDJN8ltNhJFHhFuBwL5omQ_M5WiC72xXCIMeVt4TVnLCCp_yZQ/s320/13.jpg"
                                 class="d-block w-100" alt="..."></a>
                              </div>
                            </div>
                          </div>
                      </div>
                      </div>
                    </div>
                </center><br><br>
                <center>
                    <div class="container">
                        <div class="row align-items-start">
                          <div class="col">
                            <h1>Fan Arts</h1>
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="carousel-item active"><a href="#">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg" 
                                    class="d-block w-100" alt="...">
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjs310urKzVYkXzaVoFAmXynDnbmcQUsl4bdxoqZcwd_yBMkddYaHTGnl3_hAFLxTW0VsNVy7zX_VJl0u0bQo8Xqde3pWl60zzYb22otdtTZUoYdAHuEhaJMsifcEY6QUIj2vsTub3NyiVJWdLYCHsf3XtWQJBLWQWJWAaP7GGaSP1-k9qJ6Qko5Sz1VQ/s320/1.jpg" 
                                    class="d-block w-100" alt="...">
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhM2azsEyZtffJE6zoqdcmZX4ATjls-QpfjSjehPPv0FNwTrEa-SeWOyz0Zfrje_bCsPbcizVdGmbcVCC6QgZqlDsOEFIxIHX2kNcAJ23E9ScCmPB86DvqIIMLibHXabheFdLRcyF9FXDJN8ltNhJFHhFuBwL5omQ_M5WiC72xXCIMeVt4TVnLCCp_yZQ/s320/13.jpg"
                                     class="d-block w-100" alt="..."></a>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col">
                            <h1>Digital</h1>
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="carousel-item active"><a href="#">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgCWG2TBL2-AFPFfNlVGI9KigNE4_0O_h6w_O7LrH8_qde9ucNH6blqtPy53aBiQjZAQHAZyLNpuuxsCXNpn49HHpUfOfDKpd21zPGhgHw9Ulh97roIS0dOvutVhCTIy823rvuXVE7GHZFSqjPeqx1YTxpjnKYXpiPOHevL-p2IFsqSVwC0GaGh79BeRA/s320/14.jpg" 
                                    class="d-block w-100" alt="...">
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjs310urKzVYkXzaVoFAmXynDnbmcQUsl4bdxoqZcwd_yBMkddYaHTGnl3_hAFLxTW0VsNVy7zX_VJl0u0bQo8Xqde3pWl60zzYb22otdtTZUoYdAHuEhaJMsifcEY6QUIj2vsTub3NyiVJWdLYCHsf3XtWQJBLWQWJWAaP7GGaSP1-k9qJ6Qko5Sz1VQ/s320/1.jpg" 
                                    class="d-block w-100" alt="...">
                                  </div>
                                  <div class="carousel-item">
                                    <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhM2azsEyZtffJE6zoqdcmZX4ATjls-QpfjSjehPPv0FNwTrEa-SeWOyz0Zfrje_bCsPbcizVdGmbcVCC6QgZqlDsOEFIxIHX2kNcAJ23E9ScCmPB86DvqIIMLibHXabheFdLRcyF9FXDJN8ltNhJFHhFuBwL5omQ_M5WiC72xXCIMeVt4TVnLCCp_yZQ/s320/13.jpg"
                                     class="d-block w-100" alt="..."></a>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col">
                            <h1>Ver mas</h1><a href="<?php echo e(route('cuadros')); ?>">
                            <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiN53v3a3YIsK2AlAS1gZO6m-M315jiR4mPPa5ND3ZWysls8qvJnFqaAzHzw_J_T1idBqzACo5SFLKTYFnu85Uufnt4Drp-82bqXwa1n_1nteb9ypfnBKN7oZvSvUJ_pl3CzvE1EUeQbPFCCezcAHK7LCCEjz81GbUHugoLzIj00Ebo0MVAvI0IOWqdXw/s320/VER%20MAS.png"
                            class="d-block w-100" alt="..."></a>
                          </div>
                          </div>
                        </div>
    
                </center>
             <br>
<br>
            

                <section class="pt-4" id="con2">
                  <div class="container px-lg-5">
                      <!-- Page Features-->
                      <div class="row gx-lg-5">
                          <div class="col-lg-6 col-xxl-4 mb-5">
                              <div class="card bg-light border-0 h-100">
                                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                                      <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="bi bi-collection"></i></div>
                                      <h2 class="fs-4 fw-bold">Ver cursos</h2>
                                      <p class="mb-0">Ver algunos cursos en Domestika <a href="https://www.domestika.org/es/xhr/courses/modals/138-dibujo-para-principiantes-nivel-1">AQUI</a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6 col-xxl-4 mb-5">
                              <div class="card bg-light border-0 h-100">
                                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                                      <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="bi bi-cloud-download"></i></div>
                                      <h2 class="fs-4 fw-bold">Listas de reproduccion</h2>
                                      <p class="mb-0">Ver listas de reproduccion en youtube sobre tutoriales de dibujo <a href="https://www.youtube.com/playlist?list=PLt77SgAje-ddyE_ZHVsvzNIH9hPD_4Tji">AQUI</a></p>
                                  </div>
                              </div>
                          </div>
                          </div>
                          </section>
    <script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
                    <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
                    <script>
                        window.addEventListener('load', function(){
                        new Glider(document.querySelector('.carousel__lista'), {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            dots: '.carousel__indicadores',
                            arrows: {
                                prev: '.carousel__anterior',
                                next: '.carousel__siguiente'
                            },
                            responsive: [
                                {
                                  breakpoint: 450,
                                  settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                  }
                                },{
                                  breakpoint: 800,
                                  settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1
                                  }
                                }
                            ]
                        });
                    });
                    </script>
  <?php $__env->stopSection(); ?>


</body>
</html>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/inicio.blade.php ENDPATH**/ ?>
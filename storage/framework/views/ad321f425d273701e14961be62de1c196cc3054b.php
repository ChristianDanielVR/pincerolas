<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de cuadro</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">

<!-- Styles -->
<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
<style>
        #cont{
          background-color: #373737;
         
        }

    
    </style>

</head>
<body>
    


<?php $__env->startSection('content'); ?>

<header class="container-xxl text-white shadow-lg p-3 mb-5" id="cont">
    <div class="container mt-4">
        <div class="row">
          <div class="col m-4">
            <img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
            alt="60px" class="img-thumbnail shadow-lg p-3 mb-5 bg-body rounded">
            
          </div>
          <div class="col m-4">
          <div class="card shadow-lg p-3 mb-5 bg-body rounded">
            <div class="card-body text-black" id="con2">
              <h1>Logo Pincerolas</h1><br><br>
              <h3>Precio: </h3><p>$150 MXN</p>
              <h3>Descripcion</h3>
              <p>
              Este es el logo de pincerolas el cual fue creado por Pincerolas INC. Tiene los colores de la pagina
              y son los 4 principales.   
              </p>
              <h3>Autor: </h3><p>Pincerolas INC</p>
              <h3>Categoria: </h3><p>Digital</p>
              <div class="d-flex justify-content-left">
                <a class="btn btn-outline-danger mt-auto" href="">Comprar</a>
                <a class="btn btn-outline-dark mt-2" style="margin-left: 5px;" href="<?php echo e(route('carrito')); ?>">Agregar al carrito</a>
            </div>
          </div>
          </div>


      </header> 

        
        <?php $__env->stopSection(); ?>


</body>
</html>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/Caracteristicas.blade.php ENDPATH**/ ?>
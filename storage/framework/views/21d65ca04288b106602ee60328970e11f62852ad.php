<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>¿Como funciona?</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
<!-- Styles -->
<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
<style>

    .hr:hover {
        background-color: #FF4B4B;
        color:#FFFFFF;
    }

    a {
-webkit-transition: .3s all ease;
-o-transition: .3s all ease;
transition: .3s all ease; }
a, a:hover {
text-decoration: none !important; }

.content {
height: 70vh; }

.footer-59391 {
border-top: 1px solid #efefef;
background-color: #FF4B4B;
font-family: "Poppins", sans-serif;
padding: 2.5rem 0; }
.footer-59391 .site-logo {
color: #fff; }
.footer-59391 .site-logo a {
  font-size: 30px;
  color: #000;
  font-weight: 900; }
.footer-59391 .social-icons li {
display: inline-block; }
.footer-59391 .social-icons li a {
  display: inline-block;
  position: relative;
  width: 40px;
  height: 40px;
  border-radius: 50%; }

  .footer-59391 .social-icons li a.in {
    background: #E1306C; }
  .footer-59391 .social-icons li a.fb {
    background: #3b579b; }

  .footer-59391 .social-icons li a span {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    color: #fff; }
.footer-59391 .nav-links li {
display: inline-block; }
.footer-59391 .nav-links li a {
  font-size: 15px;
  color: #efefef;
  padding: 5px }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-left li:first-child a {
  padding-left: 0; } }
.footer-59391 .nav-links.nav-right li:last-child a {
padding-right: 0; }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-right li:first-child a {
  padding-left: 0; } }
.footer-59391 .copyright {
border-top: 4px solid #efefef;
padding-top: 30px;
text-align: center;
color: #efefef; }




</style>

</head>
<body>
    
  



<?php $__env->startSection('content'); ?>

    <div id="texto" class="row justify-content-center">
      <div class="col-md-6">
        <h1 id="titulo">¿Como funciona?</h1>
        <p>Bienvenido a Pincerolas donde encontraras diferentes obras tanto a lapiz como cuadros a base de acrilicos</p>
        <p>Puede que no estes muy familiarizado con las pagina asi que aqui te enseñaremos como utilizarla y como navegar</p>
      <div>

        <div id="accordion">

          <div class="card">
            <div class="card-header">
              <a class="btn" data-bs-toggle="collapse" href="#collapseOne">
                ¿Como puedo navegar por la pagina?
              </a>
            </div>
            <div id="collapseOne" class="collapse show bg" data-bs-parent="#accordion">
              <div class="card-body">
                <p>En Pincerolas podras encontrar diversos apartados como:</p>
                <ul>
                  <li><a href="inicio.html">Categorias</a><p>Aqui puedes ver las diferentes categorias que Pincerolas ofrece</p></li>
                  
                  <li><a href="Configuración.html">Configuracion</a><p>En este apartado puedes cambiar tu informacion como la contraseña, correo electronico y cambiar tu direccion domiciliaria</p>
                    <p class="text-danger">Nota: Solo se puede acceder si tienes la sesion iniciada</p></li>
                  
                  <li><a href="carrrito.html">Carrito de compras</a><p>En este apartado puedes visualizar todos los productos que deseas comprar o eliminar de tu lista en caso de ya no quererlo</p>
                  <p class="text-danger">Nota: Solo se puede acceder si tienes la sesion iniciada</p></li>
                  
                  <li><a href="Contactanos.html">Informacion de contacto</a><p>Aqui puedes encontrar toda la informacion de contacto desde numero telefonico, correo electronico y redes sociales</p></li>
                  <li><a href="Sobre Nosotros.html">¿Quienes somos?</a><p>Aqui puedes encontrar la informacion de Pincerolas y conocer mas sobre nosotros</p></li>
                  <li><a href="login.html">Iniciar sesion</a>/<a href="registro.html">Registrarte</a><p>Aqui puedes iniciar sesion o en caso de no tener tu cuenta te puedes registrar</p>
                    <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEg0yuEn44Lj3arB5b2-DmBddozRgS6H6yQIBuGYVAfGQSrTmleWmh3IjPCscOjDz05O2bRDr3voaUqM9PRHzzozLvhPH-sQBRmAul6Y9fx50UCI6aVxuUkbOkzdGeZ2BxEEU6cWFeYsR3GEgCb2xo3dNTW3-W2hFtMjUWhMY4KVGY40fFkO4CmOO4pIUA/s320/t4.png" alt="p1" height="350px"></li>
                </ul>

              </div>
            </div>
          </div>
        

          <div class="card">
            <div class="card-header">
              <a class="collapsed btn" data-bs-toggle="collapse" href="#collapseTwo">
                ¿Como me registro?
              </a>
            </div>
            <div id="collapseTwo" class="collapse" data-bs-parent="#accordion">
              <div class="card-body">
                <p>Para registrarse en la pagina de Pincerolas es necesario llenar un formulario que pedira datos como:</p>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">Nombre completo</li>
                  <li class="list-group-item">Correo electronico</li>
                  <li class="list-group-item">Contraseña</li>
                </ul>

                <p>Una vez llenado lo necesario, damos siguiente y listo, podremos disfrutar del contenido de la pagina</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhnmmiVf_V_YEXmXCT70faCI5zknqPK4EgrBN7l-Zn3ybKj8yCD6AUvQWa2u33UeH9XlbC6KHDoZKmtMrLNP3p-TAYyYv1dAtG6xAT97EEXhQbTcS7qw4Cb1ibZktTSr6YfuUZNTU2YXQKAqxU8gVCbRfIss2064KEzScKXzJ4-7XUsQoRu-GHyCfV9Zg/s320/t3.png" alt="p1" height="350px">
                <p>Una vez registrados podra ver todas las obras que Pincerolas puede ofrecer, asi como hacer un pedido, agregar obras para su posterior venta, cambiar sus datos ingresados, etc.</p>
                <p>Si no tienes cuenta registrate  <a href="<?php echo e(route('register')); ?>">AQUI</a></p>
              </div>
            </div>
          </div>
        

          <div class="card">
            <div class="card-header">
              <a class="collapsed btn" data-bs-toggle="collapse" href="#collapseThree">
                ¿Como añado una pintura al carrito?
              </a>
            </div>
            <div id="collapseThree" class="collapse" data-bs-parent="#accordion">
              <div class="card-body">
                <p>Paso 1: Elegir la obra que le interese</p>
                <p>Paso 2: Dirigirse al boton de "Agregar al carrito"</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhLGKaqRkn59RnPyP2U8cYku1HcGGWmjr1aZqJQhpYxgNuRGKQw6AjpHFS3ScH87l-IP9Zw2t8uQGV7K104jh0Q0EcVgAGZdRdXeu7u8wlLfDK4y1kiCtzr3fiWmwh8z4o8uHTdOnsnVym9x9NuaGw8S9YyAzywGWNU2FRi5z61Ph-Jw2cYnGWyFTqHTg/s320/t1.png" alt="p1" height="350px">
                <p>Paso 3: Ir al carrito para ver la obra</p>
                <img id="p1" class="img-fluid" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhpgb4bQCPwnyvVh0-_8oECqNHyTGUMNJ5L37NvJLnDa3MLUg0dshjw8SBgorDg3-ODEyz_yTu5nFs3VHBNxEz7IXV7rkWBLrohz9XGgAS1VJajkrGZgqRUruckTmFYaidrIZF9DDBl6yS-uzUcahFUbrCnQb_B9ibqH8G4VQ_eWkK3o6gHoV_dp73lOQ/s320/t2.png" alt="p1" height="350px">
                <p>Nota: En este apartado el usuario puede comprar la obra o tambien puede eliminar todo el carrito o una sola obra</p>
              </div>
            </div>
          </div>
        
        </div>

    </div> </div> </div> </div> </div> <br><br>

    <?php $__env->stopSection(); ?>
    
</body>
</html>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/Funcionamiento.blade.php ENDPATH**/ ?>
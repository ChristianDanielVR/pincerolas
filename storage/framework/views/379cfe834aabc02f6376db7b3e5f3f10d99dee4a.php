<title>Contacto</title>
<link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
type="image/x-icon">


<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        <form action="https://formsubmit.co/christian.danielle.vega@gmail.com" method="POST" class="formContact"
                        name="formContact" id="formContact" novalidate="novalidate" align="center">
                        <input type="hidden" id="_token" name="_token" value="hMD79ypTo0FgD5Ny1r3TgyX775OL0KtACFvlH6GR">
                        <div class="d-flex justify-content-rigth">
                            <div class="col-12 col-md-10 plr-20">
                                <div class="form-group row">
                                    <div class="col-2 p-0 form-name">
                                        <span class="nameInput"> Nombre </span>
                                    </div>
                                    <div class="col-10 p-0">
                                        <span class="nombre-completo">
                                            <input type="text" align="right" name="name" id="nombre-completo" value=""
                                                size="40" class="form-control inputStyle"
                                                placeholder="Escribe tu nombre">
                                            <br>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 p-0 form-name">
                                        <span class="nameInput"> Email </span>
                                    </div>
                                    <div class="col-10 p-0">
                                        <span class="emailContacto">
                                            <input type="email" name="email" id="emailContacto" value="" size="40"
                                                class="form-control inputStyle"
                                                placeholder="Escribe tu correo electrónico">

                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-2 p-0 form-name showMobile"> <span class="nameInput">
                                            Comentarios
                                        </span></div>
                                    <div class="col-12 p-0">
                                        <span class="comentarios">
                                            <textarea name="message" id="comentarios" cols="40" rows="5"
                                                class="form-control inputStyle"
                                                placeholder="Escribe tus comentarios"></textarea>
                                            <br>
                                        </span>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center pb50SM" align="center">
                                    <button style="background-color: #FF4B4B;color:#FFFFFF;" type="submit" class="btn " role="button"
                                        aria-pressed="true">Enviar</button>
                                    <br>
                                </div>
                                
                            </div>
                        </div>
                        </form>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/contacto.blade.php ENDPATH**/ ?>
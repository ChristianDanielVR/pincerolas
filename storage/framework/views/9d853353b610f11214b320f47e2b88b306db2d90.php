<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <title>Sobre Nosotros</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!-- Styles -->
<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
<style>

    .hr:hover {
        background-color: #FF4B4B;
        color:#FFFFFF;
    }

    a {
-webkit-transition: .3s all ease;
-o-transition: .3s all ease;
transition: .3s all ease; }
a, a:hover {
text-decoration: none !important; }

.content {
height: 70vh; }

.footer-59391 {
border-top: 1px solid #efefef;
background-color: #FF4B4B;
font-family: "Poppins", sans-serif;
padding: 2.5rem 0; }
.footer-59391 .site-logo {
color: #fff; }
.footer-59391 .site-logo a {
  font-size: 30px;
  color: #000;
  font-weight: 900; }
.footer-59391 .social-icons li {
display: inline-block; }
.footer-59391 .social-icons li a {
  display: inline-block;
  position: relative;
  width: 40px;
  height: 40px;
  border-radius: 50%; }

  .footer-59391 .social-icons li a.in {
    background: #E1306C; }
  .footer-59391 .social-icons li a.fb {
    background: #3b579b; }

  .footer-59391 .social-icons li a span {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    color: #fff; }
.footer-59391 .nav-links li {
display: inline-block; }
.footer-59391 .nav-links li a {
  font-size: 15px;
  color: #efefef;
  padding: 5px }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-left li:first-child a {
  padding-left: 0; } }
.footer-59391 .nav-links.nav-right li:last-child a {
padding-right: 0; }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-right li:first-child a {
  padding-left: 0; } }
.footer-59391 .copyright {
border-top: 4px solid #efefef;
padding-top: 30px;
text-align: center;
color: #efefef; }

.body{
    margin: 0;
    padding: 0;
}

.container {
    width: 100%;
    height: 70vh;
    overflow: hidden;
}

.container .column {
    width: 25%;
    height: 100%;
    float: left;
   
    box-sizing: border-box;
    z-index: 1;
}

.container .column:last-child {
    border-right: none;
}

.container .column .content {
    position: relative;
    height: 100%;
}

.container .column .content h1 {
    position: relative;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
    background: rgba(0, 0, 0, .1);
    text-align: center;
    margin: 0;
    padding: 0;
    font-size: 2em;
    border-top: 2px solid rgba(0, 0, 0, .5);
    border-bottom: 2px solid rgba(0, 0, 0, .5);
}

.container .column .content .box {
    position: absolute;
    top: 50%;
    transform: translateY(100%);
    box-sizing: border-box;
    padding: 40px;
    background: rgb(255, 255, 255);
    text-align: center;
    transition: 0.5s;
    opacity: 0;
}

.container .column.active .content .box {
    opacity: 1;
    transform: translateY(-50%);
}

.container .column .content .box h2 {
    margin: 0;
    padding: 0;
    font-size: 30px;
    color: #FF4B4B;
}

.container .column .content .box p {
    color: #262626;
    font-size: 18px;
}

.container .column .bg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    pointer-events: none;
    z-index: -1;
}

.container .column.active .bg.bg {
   
    background-position: center;
    transition: 0.5s
}

.container .column.active .bg.bg2 {
   
    background-position: center;
    transition: 0.5s
}


.container .column.active .bg.bg3 {
    background: url(R.png);
    background-attachment: fixed;
    background-position: center;
    transition: 0.5s
}
.container .column.active .bg.bg4 {
    background: url(a.jpg);
    background-attachment: fixed;
    background-position: center;
    transition: 0.5s
}



</style>

</head>
<body>
    


<?php $__env->startSection('content'); ?>

<center>
        <h1>Sobre nostros</h1>
    </center>
    <div id=""></div>
    <div class="container shadow-lg p-3 mb-5 bg-body rounded">
        <div class="column active">
            <div class="content">
                <h1>¿Quienes somos?</h1>
                <div class="box">
                    <h2>Nosotros</h2>
                    <p>Somos una tienda en línea que ofrece obras como dibujos o pinturas únicos de la más alta calidad, con entregas en los municipios de Santa Catarina, García, Monterrey y San Pedro en Nuevo León.
                    </p>
                </div>
            </div>
            <div class="bg bg"></div>
        </div>
        <div class="column">
            <div class="content">
                <h1>Misión</h1>
                <div class="box">
                    <h2>Misión:                    </h2>
                    <p>Ser reconocida como una página de venta de pinturas y dibujos confiable en un periodo de 3 años.
                    </p>
                </div>
            </div>
            <div class="bg bg2"></div>
        </div>
        <div class="column">
            <div class="content">
                <h1>Visión</h1>
                <div class="box">
                    <h2>Visión:
                    </h2>
                    <p>• Extender el área de servicio y entregas a mas partes de la republica

                        • Integrar envíos por línea
                        
                        • Integrar diferentes funciones a los usuarios que se registren para que puedan subir sus propias obras y venderlas para cualquier publico

                    </p>
                </div>
            </div>
            <div class="bg bg3"></div>
        </div>
        <div class="column">
            <div class="content">
                <h1>Objetivo</h1>
                <div class="box">
                    <h2>Objetivo
                    </h2>
                    <p>• Entregar trabajos acordes a lo que el cliente espera
                    </p>
                </div>
            </div>
            <div class="bg bg4"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script type="text/javascript">
        $(document).on('mouseover', '.container .column', function() {
            $(this).addClass('active').siblings().removeClass('active')
        })
    </script>
    </div>

    <section class="banner"> </section>
    <script type="text/javascript">
        window.addEventListener("scroll", function() {
            var header = document.querySelector("header");
            header.classList.toggle("sticky", window.scrollY > 0);
        })
    </script>
</body>


<?php $__env->stopSection(); ?>
</html>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/SobreNosotros.blade.php ENDPATH**/ ?>
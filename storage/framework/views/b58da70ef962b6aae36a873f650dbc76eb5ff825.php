<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Home')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <style>
     
        .hr:hover {
            background-color: #FF4B4B;
            color:#FFFFFF;
        }
       
        a {
  -webkit-transition: .3s all ease;
  -o-transition: .3s all ease;
  transition: .3s all ease; }
  a, a:hover {
    text-decoration: none !important; }

.content {
  height: 70vh; }

.footer-59391 {
  border-top: 1px solid #efefef;
  background-color: #FF4B4B;
  font-family: "Poppins", sans-serif;
  padding: 2.5rem 0; }
  .footer-59391 .site-logo {
    color: #fff; }
    .footer-59391 .site-logo a {
      font-size: 30px;
      color: #000;
      font-weight: 900; }
  .footer-59391 .social-icons li {
    display: inline-block; }
    .footer-59391 .social-icons li a {
      display: inline-block;
      position: relative;
      width: 40px;
      height: 40px;
      border-radius: 50%; }
      
      .footer-59391 .social-icons li a.in {
        background: #E1306C; }
      .footer-59391 .social-icons li a.fb {
        background: #3b579b; }
      
      .footer-59391 .social-icons li a span {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        color: #fff; }
  .footer-59391 .nav-links li {
    display: inline-block; }
    .footer-59391 .nav-links li a {
      font-size: 15px;
      color: #efefef;
      padding: 5px }
  @media (max-width: 1199.98px) {
    .footer-59391 .nav-links.nav-left li:first-child a {
      padding-left: 0; } }
  .footer-59391 .nav-links.nav-right li:last-child a {
    padding-right: 0; }
  @media (max-width: 1199.98px) {
    .footer-59391 .nav-links.nav-right li:first-child a {
      padding-left: 0; } }
  .footer-59391 .copyright {
    border-top: 4px solid #efefef;
    padding-top: 30px;
    text-align: center;
    color: #efefef; }

    </style>
</head>
<body>
    <div  id="app">
        <nav style="background-color:#9B9B9B"  class="navbar navbar-expand-md navbar-light bg-gray shadow-sm">
            <div  class="container">
                
            
                <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
                    <img class="img-rounded" src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" height="95px" width="100px" alt="logoPincerolas">
                    <b><?php echo e(config('', 'Pincerolas')); ?></b>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <?php if(Auth::check()): ?>

                        <ul class="navbar-nav me-auto">

                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('cuadros.index')); ?>"><b><?php echo e(__('Cuadros')); ?></b></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('categorias.index')); ?>"><b><?php echo e(__('Categorias')); ?></b></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('carrito')); ?>"><b><?php echo e(__('Carrito')); ?></b></a>
                            </li>

                        </ul>
                    <?php endif; ?>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                    
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                            <?php if(Route::has('login')): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('login')); ?>"><b><?php echo e(__('Iniciar sesion')); ?></b></a>
                                </li>
                            <?php endif; ?>

                            <?php if(Route::has('register')): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('register')); ?>"><b><?php echo e(__('Registrarse')); ?></b></a>
                                </li>
                            <?php endif; ?>
                            <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('inicio')); ?>"><b><?php echo e(__('Inicio')); ?></b></a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <b><?php echo e(Auth::user()->name); ?></b>
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item hr" href="<?php echo e(route('configuracion')); ?>">
                                        <?php echo e(__('Configuracion de usuario')); ?>

                                    </a>
                                    <a class="dropdown-item hr" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Cerrar sesion')); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>




        <div  id="footer">
       
       <footer class="footer-59391">
         
         <div class="container">
   
           
           <div class="row mb-5">
             <div class="col-md-10">
               <div class="site-logo">
                 <a href="<?php echo e(route('home')); ?>">Pincerolas</a>
               </div>
             </div>
             <div class="col-md-2 text-md-right">
               <ul class="list-unstyled social-icons">
                 <li><a href="#" class="fb"><span class="fa fa-facebook"></span></a></li>
                 <li><a href="https://www.instagram.com/pincerolas/" class="in"><span class="fa fa-instagram"></span></a></li>
                 
               </ul>
             </div>
           </div>
   
           <div class="row mb-5">
             <div class="col-md-6 ">
               <ul class="nav-links list-unstyled nav-left">
                 <li><a href="<?php echo e(route('contacto')); ?>">Contacto</a></li>
                 <li><a href="<?php echo e(route('Envios')); ?>">Envios</a></li>
               </ul>
             </div>
             <div class="col-md-6 text-md-right">
               <ul class="nav-links list-unstyled nav-right">
                 <li><a href="<?php echo e(route('home')); ?>">Inicio</a></li>
                 <li><a href="<?php echo e(route('Terminos')); ?>">Terminos y condiciones</a></li>
                 <li><a href="<?php echo e(route('SobreNosotros')); ?>">Sobre nosotros</a></li>
                 <li><a href="<?php echo e(route('Funcionamiento')); ?>">Como usar</a></li>
                 
               </ul>
             </div>
           </div>
           <div class="row">
             <div class="col ">
               <div class="copyright">
                 <p><small>Pincerolas Copyright 2022. Todos los derechos reservados.</small></p>
               </div>
             </div>
           </div>
         
       </div>
     </footer>
    </div>
    <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/layouts/app.blade.php ENDPATH**/ ?>
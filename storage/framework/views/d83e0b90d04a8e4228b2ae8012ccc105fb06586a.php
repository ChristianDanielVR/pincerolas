<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" type="image/x-icon">
    <title>Términos y Condiciones</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!-- Styles -->
<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
<style>

    .hr:hover {
        background-color: #FF4B4B;
        color:#FFFFFF;
    }

    a {
-webkit-transition: .3s all ease;
-o-transition: .3s all ease;
transition: .3s all ease; }
a, a:hover {
text-decoration: none !important; }

.content {
height: 70vh; }

.footer-59391 {
border-top: 1px solid #efefef;
background-color: #FF4B4B;
font-family: "Poppins", sans-serif;
padding: 2.5rem 0; }
.footer-59391 .site-logo {
color: #fff; }
.footer-59391 .site-logo a {
  font-size: 30px;
  color: #000;
  font-weight: 900; }
.footer-59391 .social-icons li {
display: inline-block; }
.footer-59391 .social-icons li a {
  display: inline-block;
  position: relative;
  width: 40px;
  height: 40px;
  border-radius: 50%; }

  .footer-59391 .social-icons li a.in {
    background: #E1306C; }
  .footer-59391 .social-icons li a.fb {
    background: #3b579b; }

  .footer-59391 .social-icons li a span {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    color: #fff; }
.footer-59391 .nav-links li {
display: inline-block; }
.footer-59391 .nav-links li a {
  font-size: 15px;
  color: #efefef;
  padding: 5px }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-left li:first-child a {
  padding-left: 0; } }
.footer-59391 .nav-links.nav-right li:last-child a {
padding-right: 0; }
@media (max-width: 1199.98px) {
.footer-59391 .nav-links.nav-right li:first-child a {
  padding-left: 0; } }
.footer-59391 .copyright {
border-top: 4px solid #efefef;
padding-top: 30px;
text-align: center;
color: #efefef; }




</style>

</head>
<body>
    


<?php $__env->startSection('content'); ?>

<div class="container container--medium" style="background:#fff !important; border-radius:3px; border:1px solid #f3f3f3; margin-top:20px; margin-bottom:20px; padding:40px;">

<h1 align="center">Términos y condiciones</h1>
<br>Términos y Condiciones de Uso
<p>Por favor, lea atentamente estas Condiciones de uso. Estas Condiciones de uso regulan su acceso y su uso de este Sitio. Al acceder o utilizar este Sitio, usted está aceptando su adhesión, plena y sin reservas, a todas y cada una de estas Condiciones de uso, así como a todas las directrices, reglamentos o restricciones adicionales que puedan ser establecidas en conexión a secciones o servicios específicos de este Sitio. Todas estas directrices, reglamentos o restricciones adicionales están incorporadas por definición en las presentes Condiciones de uso.</p>

<p>La marca Pincerolas y la página https://pincerolas.com/ pertenecen a Homero Vargas. Pincerolas como marca y todos sus logotipos están protegidos por el Instituto Mexicano de Propiedad Intelectual como propiedad de Luis Fernando Cerda. Toda interacción con la página, así como su contenido, es operada por Grupo Artística Personalizada SA de CV. De aquí en adelante nos referiremos a Pincerolas, el sitio web https://pincerolas.com y sus variantes, Grupo Artística Personalizada SA de CV y todo lo que la conforma como “la empresa” o "Pincerolas" por igual.</p>

<p>Todas las imágenes que se encuentran en las diferentes páginas de https://pincerolas.com/ son propiedad de la compañía, a excepción de: Imágenes que provienen de un banco de imágenes del cual la compañía tiene derechos de uso; Imágenes proporcionadas por artistas afiliados con su previo consentimiento para la comercialización de las mismas; o imágenes que son del dominio público. Así mismo, todos los artículos y publicaciones en las páginas y blog de Pincerolas son propiedad de la empresa a menos de estar citados como propiedad de otra persona o empresa. Se prohíbe el uso de imágenes y artículos de Pincerolas a menos de contar con previa autorización escrita por parte de la empresa.</p>

<p>Los productos que aparecen y son comercializados por la empresa no son aptos para ser revendidos a menos de contar con autorización por escrito de la empresa. Los diseños gráficos de todos los productos comercializados por la empresa son propiedad de la empresa y no se permite la impresión o comercialización de estos.</p>

<p>Los precios indicados en la página ya incluyen IVA. Los precios incluyen el producto impreso solicitado por el cliente así como el envío en los casos indicados. Los precios indicados en la página incluyen solamente envíos en la república Mexicana. Los precios solamente incluyen lo que está estipulado dentro del producto que se está comprando. Es decir, no incluyen todo lo que pueda aparecer en la foto del producto a menos de que esto sea indicado como parte del producto.</p>

<p>Los tiempos de entrega de cualquier pedido son estipulados para cada producto específico, dependiendo también el método de envío o recolección seleccionado. El tiempo de entrega publicado en el carrito de compra regirá el compromiso de la empresa para todos los artículos relacionados en esa compra. En caso de existir inconsistencias con el tiempo de entrega publicado en algún otro lugar de la página, favor de reportarlo al área de atención al cliente enviando un correo a pincerolas@gmail.com o llamando al 8113662090. Estas inconsistencias no serán válidas para consideración del tiempo comprometido de entrega.</p>

<p>A través del uso de nuestra herramienta de diseño y creación de un diseño usted no recibe ningún derecho o reclamación sobre cualquiera de los elementos individuales del diseño de logotipo. Es posible que otros clientes de Pincerolas utilicen las herramientas de diseño para crear diseños con características similares o combinaciones idénticas de estos elementos de diseño y Pincerolas no garantiza que su diseño no será similar a los diseños creados y utilizados por terceros. Pincerolas no ofrece garantía de ninguna clase sobre si los diseños creados utilizando las herramientas de diseño no infringirán, o estarán sujetas a una demanda por infringir los derechos de marca registrada, propiedad intelectual o cualquier otro derecho perteneciente a un tercero. Usted es el único responsable de obtener asesoramiento de un abogado para confirmar si el diseño está legalmente disponible para su uso y si no infringe los derechos de ningún tercero. Pincerolas tampoco se hace responsable por imágenes que los usuarios pudieran agregar a sus diseños. Los usuarios son los únicos responsables de asegurarse que estas imágenes no están protegidas por la ley en México u otro país.</p>

<p>Usted confirma estar de acuerdo con que le hagamos llegar todo tipo de comunicación legal y avisos electrónicamente, bien sea en nuestro sitio web o, si Pincerolas así lo prefiriera, enviándole un correo electrónico a la dirección que nos indicó cuando se registró en nuestro sitio web. Usted podría retirar su consentimiento a recibir comunicaciones por parte nuestra de forma electrónica poniéndose en contacto con nuestro departamento de atención al cliente. En el caso de retirar dicho consentimiento, usted no podría seguir utilizando su cuenta.</p>

<p>Como usuario o cliente de Pincerolas, usted se compromete y hace responsable de cualquier reclamación o indemnización a cargo de la Sociedad y/o sus afiliados derivado de la reproducción o comercialización de imágenes, propiedad de terceros, sin previa autorización de la empresa.</p>

<p>Los pagos se pueden realizar en línea con tarjetas de débito o crédito. Los pagos se procesan en línea por las empresas PayPal, Conekta, Mercado pago, y OpenPay. Estas empresas cumplen con todos los estándares de confiabilidad y calidad estipulados en la República Mexicana y por las diferentes entidades regulatorias del comercio por internet. Los pagos en efectivo pueden ser realizados en sucursales de 7eleven, Farmacias del ahorro, Farmacias Benavides, Extra y otras tiendas de conveniencia afiliadas, así como con depósitos a la cuenta bancaría de la empresa cuando así se requiera.</p>

<p>Toda la información obtenida de los usuarios será utilizada únicamente por la empresa para brindar el servicio de la mejor manera posible. Los datos de tarjetas de crédito o débito son manejados por las empresas MercadoPago, Conekta, PayPal y/o OpenPay, y Pincerolas no tiene acceso a información sensible de esta índole. Como se mencionó en el párrafo anterior, estas empresas cumplen con todas las regulaciones Mexicanas y de entidades regulatorias del comercio por internet que aplican a nuestro país.</p>

<p>Los pedidos a la empresa se realizan mediante la plataforma web en https://pincerolas.com/. Para realizar un pedido primero se tiene que especificar datos relevantes del producto que se desea así como la dirección a donde este va a ser enviado. Al realizarse el pago de un pedido este se manda a producción. No se pueden hacer cancelaciones de pedidos una vez que fueron fabricados. Para hacer cambios o ajustes en un pedido se deben de comunicar al correo pincerolas@gmail.com. Los ajustes al pedido solamente se realizarán cuando el pedido todavía no sea fabricado, si el pedido ya fue fabricado no se podrá hacer ningún cambio a este. Cualquier excepción a esta cláusula no servirá como antecedente ni comprobante de su invalidez.</p>

<p>Pincerolas se reserva el derecho de cancelar o negar pedidos de productos que tengan el precio mal indicado en la página. Es decir, si el precio de un producto se encuentra mal indicado en la página, Pincerolas tiene el derecho de detener este pedido e informar al cliente del error que hubo. En este caso, el cliente deberá de pagar el precio correcto indicado por personal de Pincerolas, de lo contrario no se fabricará ni entregará el pedido, y el precio pagado será reembolsado al cliente a su método de pago original, o en su defecto por algún medio a convenir entre la empresa y el cliente, siempre que la empresa lo determine como un método válido y razonable.</p>

<p>Los plazos de entrega pueden variar según el producto que se compró. Los plazos de entrega se estipulan en la información del producto. El lugar de entrega del producto es en la dirección que fue indicada por el cliente que lo compró. En caso de no encontrarse al cliente o alguien que reciba el producto en dicha dirección, se tratará de entregar en 2 ocasiones más, sumando 3 intentos de entrega en 3 días consecutivos. De no tener éxito en ninguno de estos intentos de entrega, el producto será resguardado en las instalaciones más cercanas del proveedor de paquetería y logística, en este caso pueden ser DHL, FedEx, Estafeta o Redpack. El producto podrá ser enviado de vuelta a las indicaciones de Pincerolas, posterior a lo cuál la empresa no se compromete a su envío de nuevo. La decisión de volverlo a enviar, aplicar un reembolso o cualquier otro curso de acción será determinado por la empresa mediante comunicación con el cliente, buscando dentro de lo posible encontrar la mejor solución para el cliente.</p>

<p>La empresa no se hace responsable de ningún uso que se le pueda llegar a dar a los productos comercializados. La empresa tampoco se hace responsable del uso que se le pueda dar a la información o artículos que sean publicados por la misma.</p>

<p>Los datos recopilados por la empresa serán utilizados únicamente para asegurar el servicio prometido a los clientes. Los datos no serán compartidos con ningún tercero y jamás serán utilizados para fines diferentes a proveer los servicios de la empresa.</p>

<p>Los términos de uso pueden ser modificados sin previo aviso a los clientes. Los términos de uso aplicables a cada compra son los que estaban publicados en https://pincerolas.com/ en el momento de realizar dicha compra. Los clientes tendrán que acceder a estos términos de uso antes de poder completar su compra, por lo que recomendamos leerlos antes de realizar cada compra.</p>

</div>

<?php $__env->stopSection(); ?>
</body>

</html>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/Terminos.blade.php ENDPATH**/ ?>
<div class="box box-info padding-1">
    <div class="box-body">
        
        


        
        <div class="form-group">
            <?php echo e(Form::label('categoria_id')); ?>

            <?php echo e(Form::select('categoria_id',$categorias, $cuadro->categoria_id, ['class' => 'form-control' . ($errors->has('categoria_id') ? ' is-invalid' : ''), 'placeholder' => 'Categoria Id'])); ?>

            <?php echo $errors->first('categoria_id', '<div class="invalid-feedback">:message</div>'); ?>

        </div>

        <div class="form-group">
            <?php echo e(Form::label('nombre')); ?>

            <?php echo e(Form::text('nombre', $cuadro->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre'])); ?>

            <?php echo $errors->first('nombre', '<div class="invalid-feedback">:message</div>'); ?>

        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/cuadro/form.blade.php ENDPATH**/ ?>


<?php if( session('updateClave') ): ?>
<div class="alert alert-success" role="alert">
  <strong>Felicitaciones </strong>
    <?php echo e(session('updateClave')); ?>

</div>



    <div class="row justify-content-center">
        <div class="col-md-12 mt-5 text-center">
          <img src="https://toppng.com/uploads/preview/hd-green-round-tick-check-mark-vector-icon-11641942001dzorrl7w7j.png" style="width:100px;" class="img-fluid" alt="Responsive image">
        </div>
    </div>
<?php endif; ?>


<?php if( session('name') ): ?>
<div class="alert alert-success" role="alert">
  <strong>Felicitaciones </strong>
    <?php echo e(session('name')); ?>

</div>
<?php endif; ?>


<?php if( session('claveIncorrecta') ): ?>
  <div class="alert alert-danger" role="alert">
    <strong>Lo siento!</strong>  <?php echo e(session('claveIncorrecta')); ?>

  </div>
<?php endif; ?>


<?php if( session('clavemenor') ): ?>
<div class="alert alert-warning" role="alert">
  <strong>Lo siento !</strong>
    <?php echo e(session('clavemenor')); ?>

</div>
<?php endif; ?><?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/msjs.blade.php ENDPATH**/ ?>
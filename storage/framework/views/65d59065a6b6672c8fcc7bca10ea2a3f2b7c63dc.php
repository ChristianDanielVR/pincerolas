
<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('footer.name')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <style>
     
        
    </style>
</head>
<body>
    <div  id="footer">
       
    <footer class="footer-59391">
      
      <div class="container">

        
        <div class="row mb-5">
          <div class="col-md-4">
            <div class="site-logo">
              <a href="#">Colorlib</a>
            </div>
          </div>
          <div class="col-md-8 text-md-right">
            <ul class="list-unstyled social-icons">
              <li><a href="#" class="fb"><span class="icon-facebook"></span></a></li>
              <li><a href="#" class="tw"><span class="icon-twitter"></span></a></li>
              <li><a href="#" class="in"><span class="icon-instagram"></span></a></li>
              <li><a href="#" class="be"><span class="icon-behance"></span></a></li>
              <li><a href="#" class="dr"><span class="icon-dribbble"></span></a></li>
              <li><a href="#" class="yt"><span class="icon-play"></span></a></li>
            </ul>
          </div>
        </div>

        <div class="row mb-5">
          <div class="col-md-6 ">
            <ul class="nav-links list-unstyled nav-left">
              <li><a href="#">Privacy</a></li>
              <li><a href="#">Policy</a></li>
            </ul>
          </div>
          <div class="col-md-6 text-md-right">
            <ul class="nav-links list-unstyled nav-right">
              <li><a href="#">Home</a></li>
              <li><a href="#">Our works</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col ">
            <div class="copyright">
              <p><small>Copyright 2019. All Rights Reserved.</small></p>
            </div>
          </div>
        </div>
      
    </div>
  </footer>






        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\sistema\example-app\resources\views/layouts/footer.blade.php ENDPATH**/ ?>
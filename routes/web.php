<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/configuracion', function () {
    return view('configuracion');
});

Auth::routes();

Route::resource('cuadros', App\Http\Controllers\CuadroController::class)->middleware('auth');
Route::resource('categorias', App\Http\Controllers\CategoriaController::class)->middleware('auth');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/configuracion', [App\Http\Controllers\UsersettingsController::class, 'index'])->name('configuracion');
Route::get('/change/password', [App\Http\Controllers\UsersettingsController::class, 'changePassword'])->name('changePassword');
Route::get('/Catalogo', [App\Http\Controllers\VistacuadrosController::class, 'index'])->name('cuadros');
Route::get('/Sobre Nosotros', [App\Http\Controllers\footer\SobrenosotrosController::class, 'index'])->name('SobreNosotros');
Route::get('/Como funciona', [App\Http\Controllers\footer\FuncionamientoController::class, 'index'])->name('Funcionamiento');
Route::get('/Caracteristicas', [App\Http\Controllers\footer\CatalogoController::class, 'index'])->name('Caracteristicas');
Route::get('/Terminos y condiciones', [App\Http\Controllers\footer\TerminosController::class, 'index'])->name('Terminos');
Route::get('/Envios', [App\Http\Controllers\footer\EnviosController::class, 'index'])->name('Envios');
Route::get('/Contacto', [App\Http\Controllers\footer\ContactoController::class, 'index'])->name('contacto');
Route::get('/Carrito', [App\Http\Controllers\CarritoController::class, 'index'])->name('carrito');
Route::get('/Inicio', [App\Http\Controllers\InicioController::class, 'index'])->name('inicio');


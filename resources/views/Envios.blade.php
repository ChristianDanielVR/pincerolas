<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">

  <title>Envíos</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <style>
    .hr:hover {
      background-color: #FF4B4B;
      color: #FFFFFF;
    }

    a {
      -webkit-transition: .3s all ease;
      -o-transition: .3s all ease;
      transition: .3s all ease;
    }

    a,
    a:hover {
      text-decoration: none !important;
    }

    .content {
      height: 70vh;
    }

    .footer-59391 {
      border-top: 1px solid #efefef;
      background-color: #FF4B4B;
      font-family: "Poppins", sans-serif;
      padding: 2.5rem 0;
    }

    .footer-59391 .site-logo {
      color: #fff;
    }

    .footer-59391 .site-logo a {
      font-size: 30px;
      color: #000;
      font-weight: 900;
    }

    .footer-59391 .social-icons li {
      display: inline-block;
    }

    .footer-59391 .social-icons li a {
      display: inline-block;
      position: relative;
      width: 40px;
      height: 40px;
      border-radius: 50%;
    }

    .footer-59391 .social-icons li a.in {
      background: #E1306C;
    }

    .footer-59391 .social-icons li a.fb {
      background: #3b579b;
    }

    .footer-59391 .social-icons li a span {
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      color: #fff;
    }

    .footer-59391 .nav-links li {
      display: inline-block;
    }

    .footer-59391 .nav-links li a {
      font-size: 15px;
      color: #efefef;
      padding: 5px
    }

    @media (max-width: 1199.98px) {
      .footer-59391 .nav-links.nav-left li:first-child a {
        padding-left: 0;
      }
    }

    .footer-59391 .nav-links.nav-right li:last-child a {
      padding-right: 0;
    }

    @media (max-width: 1199.98px) {
      .footer-59391 .nav-links.nav-right li:first-child a {
        padding-left: 0;
      }
    }

    .footer-59391 .copyright {
      border-top: 4px solid #efefef;
      padding-top: 30px;
      text-align: center;
      color: #efefef;
    }
  </style>

</head>

<body>

@extends('layouts.app')

@section('content')

  <div class="container container--medium"
    style="background:#fff !important; border-radius:3px; border:1px solid #f3f3f3; margin-top:20px; margin-bottom:20px; padding:40px;">

    <h1 align="center">Envíos, tarifas y lugares</h1>
    <br>
    <p>
      El envío de nuestros cuadros a México y todo el mundo es gratuito desde Madrid, España para compras de $11,250 o
      más. Independientemente del número de cuadros y el lugar de destino. Para compras inferiores la tarifa fija es de
      $1,058 por envío, independientemente del lugar y el número de cuadros.
    </p>
    <p>
      Obras originales: enviamos en 24-48 horas.
    </p>

    <p>
      Reproducciones al óleo: Para regalarlas le enviaremos dentro de 2 días hábiles el certificado de compra con el
      nombre y la imagen del cuadro que estamos pintando para usted, saber más. El tiempo aproximado de envío es de 5-8
      semanas desde la orden.
    </p>

    <p>
      Utilizamos cajas acolchadas para trabajos enmarcados y tubos para posters, cuadros y dibujos que lo permitan, ambos
      embalajes son de alta resistencia y minimizan las posibilidades de que el producto sufra daños, sin embargo si su
      envío viene dañado, se lo cambiaremos totalmente gratis.

    </p>


  

  </div>

</body>
@endsection

</html>
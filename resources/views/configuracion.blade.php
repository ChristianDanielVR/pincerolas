<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    
  <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhSr2vQNbqQpyXQciIGAtD0iDNkra1wpQ3BOJjlAf4tXDuLmiwTEpbpv_4ESeApndXQq-SF7bAAz59DVWH32knUtWKsxk4vI1Kbz3EDP9MoHkd8wxV0Njpd2ZZKFbeGnt9Oip1eXHjgUPRKv-C04GcSYYJxRGBQESsJV-5-E2UEszaje-dfuWkvcTN-9A/s320/logo.png" 
    type="image/x-icon">
    <title>Configuración</title>
</head>
<body>



@extends('layouts.app')

@section('content')
<link rel="icon" href="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgqgvgoh5wuPsyOum8qqwwNg8UyUZOI7EElvT29OfbbTRmZFK8CojXgYyrjO1QhYU5VEaAB5xDPmlcHY2pa_1quuZ2YmhwytCYqvWMu2Iry9oDHJVLxQoxHJh8faS9CmA54pmGGYtpZIsorAgL0_rjVxCYFNyVtKiXvd7lkJ1wMJrF366KBRn6GtyEjzg/w320-h320/logopincerolas.png" type="image/x-icon">
<div class="container mb-5" style="background-color: #fff;">

<!--- Mensajes -->
@include('msjs')


  <h2 class="text-center">Actualizar datos de usuario <hr></h2>
  <div class="row justify-content-center">
      <div class="col-md-8">
        <form action="{{route('changePassword')}}" method="GET" class="needs-validation" novalidate>
            @csrf 

            <div class="row mb-3">
                <div class="form-group mt-3">
                    <label for="name">Nombre de Usuario</label>
                    <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control @error('name') is-invalid @enderror" required>
                      @error('name')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
              </div>    
            <div class="row mb-3">
              <div class="form-group mt-3">
                  <label for="password_actual"> Contraseña actual</label>
                  <input type="password" name="password_actual" class="form-control @error('password_actual') is-invalid @enderror" required>
                    @error('password_actual')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="form-group mt-3">
                    <label for="new_password ">Nueva contraseña</label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" required>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="form-group mt-3">
                    <label for="confirm_password">Confirmar nueva contraseña</label>
                    <input type="password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror"required>
                    @error('confirm_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="row text-center mb-4 mt-5">
                <div class="col-md-12">
                    <button type="submit" style="background-color:#FF4B4B;color:#fff" class="btn " id="formSubmit">Guardar Cambios</button>
                    <a href="/home" class="btn btn-secondary">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>


@endsection
</html>

